import * as fromPlayers from '../actions/players.actions';
import { Player } from 'src/app/models/player';


export interface PlayerState {
    entities: { [id: number] : Player};
    loaded: boolean;
    loading: boolean;
}

export const initialState: PlayerState = 
    {
        entities: {},
        loaded: false,
        loading: false
    }
    
    export default function (state = initialState, action: fromPlayers.PlayersAction): PlayerState{
        switch(action.type){
            case fromPlayers.LOAD_PLAYERS: {
                return {
                    ...state, 
                    loading: true
                };
            }
            case fromPlayers.LOAD_PLAYERS_SUCCESS: {
                const  players  = action as fromPlayers.LoadPlayersSuccess;
    
                const entities = players.players.reduce((entities: { [id: number]: Player}, player: Player) => {
                    return {
                        ...entities, 
                        [player.id]: player
                    }
                },
                {
                    ...state.entities
                })
                return{
                    ...state,
                    loading: false,
                    loaded: true,
                    entities
                }
            }
            case fromPlayers.LOAD_PLAYERS_FAIL: {
                return {
                    ...state, 
                    loading: false,
                    loaded: false
                };
            }
            default: 
                return state;
        }
    }

    export const getPlayersLoading = (state: PlayerState) => state.loading;
    export const getPlayersLoaded = (state: PlayerState) => state.loaded;
    export const getPlayersEntities = (state: PlayerState) => state.entities;