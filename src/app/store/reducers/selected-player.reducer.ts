import * as fromPlayers from '../actions/players.actions';
import { Player } from 'src/app/models/player';
import { Action } from '@ngrx/store';


export default function (state: Player = null, action: Action) {
    switch(action.type) {
        case fromPlayers.SELECT_PLAYER: {
            return (action as fromPlayers.SelectPlayer).player;
        }
        case fromPlayers.UPDATE_PLAYER: {
            const {player} = action as fromPlayers.UpdatePlayer;
            return state && player.id === state.id?
                player:
                state;
        }
        default:
            return state;
    }
}