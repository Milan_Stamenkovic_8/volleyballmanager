import { Manager } from "src/app/models/manager";
import * as fromManagers from '../actions/managers.actions';


export interface ManagerState {
    entities: { [id: string] : Manager};
    loaded: boolean;
    loading: boolean;
}

export const initialState: ManagerState = 
    {
        entities: {},
        loaded: false,
        loading: false
    }
    



export default function (state = initialState, action: fromManagers.ManagersAction): ManagerState{
    switch(action.type){
        case fromManagers.LOAD_MANAGERS: {
            return {
                ...state, 
                loading: true
            };
        }
        case fromManagers.LOAD_MANAGERS_SUCCESS: {
            const  managers  = action as fromManagers.LoadManagersSuccess;

            const entities = managers.managers.reduce((entities: { [id: string]: Manager}, manager: Manager) => {
                return {
                    ...entities, 
                    [manager.id]: manager
                }
            },
            {
                ...state.entities
            })
            return{
                ...state,
                loading: false,
                loaded: true,
                entities
            }
        }
        case fromManagers.LOAD_MANAGERS_FAIL: {
            return {
                ...state, 
                loading: false,
                loaded: false
            };
        }
        case fromManagers.ADD_MANAGER: {
            const { manager } = action as fromManagers.AddManager;
            return{
                ...state,
                ...state.entities[manager.id]
            }
        }
        case fromManagers.LOGIN_SUCCESS: {
            const { id } = action as fromManagers.LoginSucces;
            
            return{
                ...state,
                ...state.entities[id]
            }
        }
        default: 
            return state;
    }
}

export const getManagersLoading = (state: ManagerState) => state.loading;
export const getManagersLoaded = (state: ManagerState) => state.loaded;
export const getManagersEntities = (state: ManagerState) => state.entities;