import { ActionReducerMap, createSelector, createFeatureSelector } from '@ngrx/store';
import * as fromManagers from './managers.reducer';
import * as fromPlayers from './players.reducer';
import * as fromSelcetedPlayer from './selected-player.reducer';
import { Player } from 'src/app/models/player';

export interface VolleyState {
    managers: fromManagers.ManagerState;
    players: fromPlayers.PlayerState;
    selectedPlayer: Player
}

export const reducers: ActionReducerMap<VolleyState> = {
    managers: fromManagers.default,
    players: fromPlayers.default,
    selectedPlayer: fromSelcetedPlayer.default
};

export const getVolleyState = createFeatureSelector<VolleyState>('volley');

export const getManagerState = createSelector(getVolleyState, (state: VolleyState) => state.managers);
export const getManagersEntities = createSelector(getManagerState, fromManagers.getManagersEntities);
export const getAllManagers = createSelector(getManagersEntities,  (entities) => { return Object.keys(entities).map(id => entities[id])})
export const getManagersLoading = createSelector(getManagerState, fromManagers.getManagersLoading);
export const getManagersLoaded = createSelector(getManagerState, fromManagers.getManagersLoaded);

export const getPlayerState = createSelector(getVolleyState, (state: VolleyState) => state.players);
export const getPlayersEntities = createSelector(getPlayerState, fromPlayers.getPlayersEntities);
export const getAllPlayers = createSelector(getPlayersEntities,  (entities) => { return Object.keys(entities)
                                            .map(id => entities[parseInt(id)])})
export const getPlayersLoading = createSelector(getPlayerState, fromPlayers.getPlayersLoading);
export const getPlayersLoaded = createSelector(getPlayerState, fromPlayers.getPlayersLoaded);