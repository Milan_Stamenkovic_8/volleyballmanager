import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import * as managerActions from '../actions/managers.actions';
import { switchMap, map, catchError, exhaustMap } from 'rxjs/operators';
import * as fromServices from '../../service/managers.service';
import { of } from 'rxjs/internal/observable/of';

@Injectable()
export class ManagersEffects {
    constructor(private actions$: Actions, private managerService: fromServices.ManagersService){
    }

    @Effect()
    loadManagers$ = this.actions$.pipe(
        ofType(managerActions.LOAD_MANAGERS),
        switchMap(() => {
            return this.managerService.getManagers().pipe(
                map(managers => new managerActions.LoadManagersSuccess(managers)),
                catchError(error => of(new managerActions.LoadManagersFailure(error)))
            )
        })
    );

     @Effect()
    addManager$ = this.actions$.pipe(
        ofType(managerActions.ADD_MANAGER),
        map((action: managerActions.AddManager) => action.manager),
        exhaustMap((request) => {
            return this.managerService.addManager(request)
        })
    )

}