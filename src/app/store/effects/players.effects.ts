import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import * as playerActions from '../actions/players.actions';
import { switchMap, map, catchError, exhaustMap } from 'rxjs/operators';
import * as fromServices from '../../service/players.service';
import { of } from 'rxjs/internal/observable/of';

@Injectable()
export class PlayersEffects {
    constructor(private actions$: Actions, private playerService: fromServices.PlayersService){
    }

    @Effect()
    loadManagers$ = this.actions$.pipe(
        ofType(playerActions.LOAD_PLAYERS),
        switchMap(() => {
            return this.playerService.getPlayers().pipe(
                map(players => new playerActions.LoadPlayersSuccess(players)),
                catchError(error => of(new playerActions.LoadPlayersSuccess(error)))
            )
        })
    )
}