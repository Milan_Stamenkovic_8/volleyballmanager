import { ManagersEffects } from './managers.effects';
import { PlayersEffects } from './players.effects';

export const effects: any[] = [ManagersEffects, PlayersEffects];

export * from './managers.effects';
export * from './players.effects';