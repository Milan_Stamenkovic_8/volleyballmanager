import { Action } from '@ngrx/store';
import { Player } from 'src/app/models/player';


export const LOAD_PLAYERS = "[PLAYER] Load Players";
export const LOAD_PLAYERS_SUCCESS = "[PLAYER] Load Players Success";
export const LOAD_PLAYERS_FAIL = "[PLAYER] Load Players Fail";
export const SELECT_PLAYER = "[PLAYER] Select Player";
export const UPDATE_PLAYER = "[PLAYER] Update Player";

export class LoadPlayers implements Action {
    type = LOAD_PLAYERS;
}

export class LoadPlayersSuccess implements Action {
    type = LOAD_PLAYERS_SUCCESS;
    constructor(public players: Player[]) {
        this.players =  players ;
    }
}

export class LoadPlayersFailure implements Action {
    type = LOAD_PLAYERS_FAIL;
    constructor(public player: string) {
        this.player = player;
    }
}

export class SelectPlayer implements Action {
    type = SELECT_PLAYER;
    constructor(public player: Player){
    }
}

export class UpdatePlayer implements Action {
    type = UPDATE_PLAYER;
    player: Player;
    constructor(player: Player) {
        this.player = {...player};
    }
}

export type PlayersAction =  LoadPlayers | LoadPlayersSuccess | LoadPlayersFailure | UpdatePlayer;