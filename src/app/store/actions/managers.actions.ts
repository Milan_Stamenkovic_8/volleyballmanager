import { Action } from '@ngrx/store';
import { Manager } from 'src/app/models/manager';


export const LOAD_MANAGERS = "[USER] Load Managers";
export const LOAD_MANAGERS_SUCCESS = "[USER] Load Managers Success";
export const LOAD_MANAGERS_FAIL = "[USER] Load Managers Fail";
export const ADD_MANAGER = "[USER] Add Manager";
export const LOGIN_SUCCESS = "[USER] Login Success";


export class LoadManagers implements Action {
    type = LOAD_MANAGERS;
}

export class LoadManagersSuccess implements Action {
    type = LOAD_MANAGERS_SUCCESS;
    constructor(public managers: Manager[]) {
        this.managers =  managers ;
    }
}

export class LoadManagersFailure implements Action {
    type = LOAD_MANAGERS_FAIL;
    constructor(public manager: string) {
        this.manager = manager;
    }
}

export class AddManager implements Action {
    type = ADD_MANAGER;
    constructor(public manager: Manager) {
        this.manager = {...manager};
    }
}

export class LoginSucces implements Action {
    type = LOGIN_SUCCESS;
    constructor(public id: string){
        this.id = id;
    }
}




export type ManagersAction = LoadManagers | LoadManagersSuccess | LoadManagersFailure | 
                             AddManager | LoginSucces;