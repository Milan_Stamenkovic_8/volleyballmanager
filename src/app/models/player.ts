export class Player {
    constructor(
    public id: number,
    public playerName: string,
    public playerLastName: string,
    public role: string,
    public country: string,
    public team: string,
    public height: number,
    public age: number
    ){}
}