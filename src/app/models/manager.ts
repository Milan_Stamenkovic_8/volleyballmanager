export class Manager {
    constructor(
    public id: string,
    public managerName: string,
    public password: string,
    public title: string,
    public careerPoints: number,
    public privileges: string
    ){}
}