import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Manager } from '../models/manager';
import { catchError } from 'rxjs/operators';

const managersUrl = "http://localhost:3000/managers";

@Injectable({
  providedIn: 'root'
})

export class ManagersService {

  constructor(private http: HttpClient) { }

  public getManagers(): Observable<Manager[]>
  {
    return this.http.get<Manager[]>(managersUrl)
    .pipe(catchError((error: any) => Observable.throw(error.json())));
  }

  public addManager(manager: Manager): Observable<Manager>
  {
    return this.http.post<Manager>(managersUrl, manager)
    .pipe(catchError((error: any) => Observable.throw(error.json())));
  }
}
