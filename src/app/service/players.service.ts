import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Player } from '../models/player';
import { catchError } from 'rxjs/operators';

const playersUrl = "http://localhost:3000/players";

@Injectable({
  providedIn: 'root'
})
export class PlayersService {

  constructor(private http: HttpClient) { }

  public getPlayers(): Observable<Player[]>
  {
    return this.http.get<Player[]>(playersUrl)
    .pipe(catchError((error: any) => Observable.throw(error.json())));
  }
}
