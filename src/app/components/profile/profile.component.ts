import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Manager } from 'src/app/models/manager';
import { Store } from '@ngrx/store';
import * as fromStore from '../../store';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  managers$: Observable<Manager[]>;

  constructor(private store: Store<fromStore.VolleyState>) { }

  ngOnInit() {
    this.managers$ = this.store.select(fromStore.getAllManagers);
  }


}
