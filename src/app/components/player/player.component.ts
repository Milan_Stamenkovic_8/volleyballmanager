import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Player } from 'src/app/models/player';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {

  @Input() public player: Player;
  @Input() public selected: boolean;
  @Output() public selectedEvent: EventEmitter<Player> = new EventEmitter();


  constructor() { 
    this.selected = false;
  }

  ngOnInit() {
  }

  public selectPlayer() {
    this.selectedEvent.emit(this.player);
  }

}
