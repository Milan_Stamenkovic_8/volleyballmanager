import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Player } from 'src/app/models/player';
import { Store } from '@ngrx/store';
import * as fromStore from '../../store';
import { SelectPlayer, UpdatePlayer } from '../../store';

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.scss']
})
export class PlayerListComponent implements OnInit {
  players$: Observable<Player[]>;
  selectedPlayer: Player;
  
  constructor(private store: Store<fromStore.VolleyState>) { }

  ngOnInit() {
    this.players$ = this.store.select(fromStore.getAllPlayers);
    this.store.dispatch(new fromStore.LoadPlayers());
  }


  onSelected(player: Player) {
    this.store.dispatch(new SelectPlayer(player));
  }

  onMovieChanged(player: Player) {
    this.store.dispatch(new UpdatePlayer(player));
  }
}
