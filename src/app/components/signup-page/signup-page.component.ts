import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Manager } from 'src/app/models/manager';
import { Store } from '@ngrx/store';
import * as fromStore from '../../store';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.scss']
})
export class SignupPageComponent implements OnInit {
  managerName: string;
  password: string;
  managers$: Observable<Manager[]>;
  signupName: boolean = false;
  
  constructor(private store: Store<fromStore.VolleyState>, private router: Router) { }

  ngOnInit() {
    this.managers$ = this.store.select(fromStore.getAllManagers);
    this.store.dispatch(new fromStore.LoadManagers());
  }

  signUp(){
    if(this.managerName === undefined || this.managerName === ""){
      alert("Please enter your Username.")
    }
    else{
      if(this.password === undefined || this.password === ""){
        alert("Please enter your password.")
      }
      else{
        this.checkUser();
        if(this.signupName === true){
          alert("Username is already taken!")
          this.signupName = false;
        }
        else{
          this.signUpSuccess();
        }
      }
    }  
  }

  checkUser(){
    this.managers$.subscribe(managers => {
      managers.forEach(manager => {
        if(this.managerName === manager["managerName"]){
          this.signupName = true;
        }
      });
    })
  }

  signUpSuccess(){
    this.store.dispatch(new fromStore.AddManager
    (new Manager(this.managerName.toLocaleLowerCase(), this.managerName, this.password, "Begginer", 0, "regular")));
    this.router.navigateByUrl('/');
  }

  getManagerName(event: any){
    this.managerName = event.target.value;
  }

  getPassword(event: any){
    this.password = event.target.value;
  }
  
}
