import { Component, OnInit, Output } from '@angular/core';
import { Manager } from 'src/app/models/manager';
import { Store } from '@ngrx/store';
import * as fromStore from '../../store';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';



@Component({
  selector: 'app-welcome-page',
  templateUrl: './welcome-page.component.html',
  styleUrls: ['./welcome-page.component.scss']
})

export class WelcomePageComponent implements OnInit {
  @Output() public managerName: string;
  password: string;
  managers$: Observable<Manager[]>;
  logginName: boolean = false;
  logginPassword: boolean = false;

  constructor(private store: Store<fromStore.VolleyState>, private router: Router) { }


  ngOnInit() {
    this.managers$ = this.store.select(fromStore.getAllManagers);
    this.store.dispatch(new fromStore.LoadManagers());  
  }

  logIn(){
    if(this.managerName === undefined || this.managerName === ""){
      alert("Please enter your Username.")
    }
    else{
      if(this.password === undefined || this.password === ""){
        alert("Please enter your password.")
      }
      else{
        this.checkUser();
        if(this.logginName === true && this.logginPassword === true){
          this.userAuthenticationSuccess();
        }
        else{
          this.userAuthenticationFail();
        }
      }
    }  
  }

  userAuthenticationSuccess(){
    this.store.dispatch(new fromStore.LoginSucces(this.managerName.toLocaleLowerCase()));
    this.router.navigateByUrl('/home');
  }

  userAuthenticationFail(){
    if(this.logginName === false){
      alert("Wrong username!");
    }
    else{
      if(this.logginPassword === false){
        alert("Wrong password!");
      }
    }

  }

  checkUser(){
    this.managers$.subscribe(managers => {
      managers.forEach(manager => {
        if(this.managerName === manager["managerName"]){
          this.logginName = true;
        }
        if(this.logginName === true && this.password === manager["password"]){
          this.logginPassword = true;
        }
      });
    })
  }


  getManagerName(event: any){
    this.managerName = event.target.value;
  }

  getPassword(event: any){
    this.password = event.target.value;
  }

}
