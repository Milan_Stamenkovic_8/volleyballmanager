import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Player } from 'src/app/models/player';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';


@Component({
  selector: 'app-player-details',
  templateUrl: './player-details.component.html',
  styleUrls: ['./player-details.component.scss']
})
export class PlayerDetailsComponent implements OnInit {
  

  @Input() public player: Player;
  @Output() changePlayer: EventEmitter<Player> = new EventEmitter<Player>();

  constructor() { }

  ngOnInit() {
  }

  onNameChange(newName: string) {
    this.changePlayer.emit({...this.player, playerName: newName});
  }
}
