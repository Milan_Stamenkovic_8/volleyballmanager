import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomePageComponent } from './components/welcome-page/welcome-page.component';
import { SignupPageComponent } from './components/signup-page/signup-page.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { ProfileComponent } from './components/profile/profile.component';
import { HelpComponent } from './components/help/help.component';
import { PlayerListComponent } from './components/player-list/player-list.component';



const routes: Routes = [
  { path: '', component: WelcomePageComponent},
  { path: 'signup', component: SignupPageComponent},
  { path: 'home', component: HomePageComponent},
  { path: 'profile', component: ProfileComponent},
  { path: 'myteam', component: PlayerListComponent},
  { path: 'help', component: HelpComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
